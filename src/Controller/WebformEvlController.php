<?php

namespace Drupal\webform_evl\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for webform element values limit.
 */
class WebformEvlController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The webform request handler.
   *
   * @var \Drupal\webform\WebformRequestInterface
   */
  protected $requestHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requestHandler = $container->get('webform.request');
    return $instance;
  }

  /**
   * Returns the summary table.
   */
  public function index() {
    $build = [];

    $handlers = $this
      ->requestHandler
      ->getCurrentWebform()
      ->getHandlers('webform_evl');
    foreach ($handlers as $handler) {
      /** @var \Drupal\webform_evl\Plugin\WebformEvlHandlerInterface $handler */
      $build[$handler->getHandlerId()] = $handler->buildSummaryTable();
      $build[$handler->getHandlerId()]['#suffix'] = '<br/><br/>';
    }

    return $build;
  }

}
