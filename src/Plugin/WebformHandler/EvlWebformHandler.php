<?php

namespace Drupal\webform_evl\Plugin\WebformHandler;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_evl\Plugin\WebformEvlHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform element values limit handler.
 *
 * @WebformHandler(
 *   id = "webform_evl",
 *   label = @Translation("Element values limit"),
 *   category = @Translation("Limits"),
 *   description = @Translation("Place a limit on the number of values an element can accept across submissions."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class EvlWebformHandler extends WebformHandlerBase implements WebformEvlHandlerInterface {

  /**
   * The database object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * The source entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $sourceEntity = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'element_key' => '',
      'limit' => NULL,
      'none_remaining_action' => WebformEvlHandlerInterface::LIMIT_ACTION_DISABLE,
      'message_display' => WebformEvlHandlerInterface::MESSAGE_DISPLAY_LABEL,
      'multiple_remaining_message' => '(@remaining remaining)',
      'single_remaining_message' => '(@remaining remaining)',
      'none_remaining_message' => '(@remaining remaining - no more @label are available)',
      'validation_error_message' => 'Only @remaining @label remain. Please choose @remaining or fewer @label.',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $settings = $this->getSettings();
    $element = $this->getElement();
    if ($element) {
      $webform_element = $this->getWebformElement();
      $t_args = [
        '@title' => $webform_element->getAdminLabel($element),
        '@type' => $webform_element->getPluginLabel(),
        '@limit' => $settings['limit'],
      ];
      $settings['element_key'] = $this->t('@title (@type)', $t_args);
    }
    elseif (empty($settings['element_key'])) {
      $settings['element_key'] = $this->t('No multi-value element configured.');
    }
    else {
      $settings['element_key'] = [
        '#markup' => $this->t("'@element_key' is missing.", ['@element_key' => $settings['element_key']]),
        '#prefix' => '<b class="color-error">',
        '#suffix' => '</b>',
      ];
    }

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Get multi-value elements.
    $multi_value_elements = $this->getMultiValueElements();

    // Make sure that there is a multi-value element available.
    if (empty($multi_value_elements)) {
      $form['message'] = [
        '#type' => 'webform_message',
        '#message_type' => 'warning',
        '#message_message' => [
          'message' => [
            '#markup' => $this->t('No multi-value elements are available.'),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          ],
          'link' => [
            '#type' => 'link',
            '#title' => $this->t('Please add or configure an element to accept multiple values.'),
            '#url' => $this->getWebform()->toUrl('edit-form'),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          ],
        ],
      ];
      return $form;
    }

    // Limit settings.
    $form['limit_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Limit settings'),
      '#open' => TRUE,
    ];
    $form['limit_settings']['element_key'] = [
      '#type' => 'select',
      '#title' => $this->t('Element'),
      '#options' => $multi_value_elements,
      '#default_value' => $this->configuration['element_key'],
      '#required' => TRUE,
      '#empty_option' => (empty($this->configuration['element_key'])) ? $this->t('- Select -') : NULL,
    ];
    $form['limit_settings']['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Values limit'),
      '#min' => 0,
      '#required' => TRUE,
      '#default_value' => $this->configuration['limit'],
    ];
    $form['limit_settings']['none_remaining_action'] = [
      '#type' => 'select',
      '#title' => $this->t('Limit reached (zero values remaining) behavior'),
      '#options' => [
        WebformEvlHandlerInterface::LIMIT_ACTION_DISABLE => $this->t('Disable the element'),
        WebformEvlHandlerInterface::LIMIT_ACTION_REMOVE => $this->t('Remove the element'),
        WebformEvlHandlerInterface::LIMIT_ACTION_NONE => $this->t('Do not alter the element'),
      ],
      '#default_value' => $this->configuration['none_remaining_action'],
    ];

    // Message settings.
    $form['message_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Message settings'),
    ];
    $form['message_settings']['message_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Message display'),
      '#options' => [
        WebformEvlHandlerInterface::MESSAGE_DISPLAY_LABEL => $this->t("Append message to the element's label"),
        WebformEvlHandlerInterface::MESSAGE_DISPLAY_DESCRIPTION => $this->t("Append message to the element's description"),
        WebformEvlHandlerInterface::MESSAGE_DISPLAY_NONE => $this->t('Do not display a message'),
      ],
      '#default_value' => $this->configuration['message_display'],
    ];
    $form['message_settings']['details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="settings[message_display]"]' => ['!value' => 'none'],
        ],
      ],
    ];
    $form['message_settings']['details']['multiple_remaining_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Multiple values remaining message'),
      '#description' => $this->t('This message is displayed when there are multiple remaining values for the element.')
      . '<br/><br/>'
      . $this->t('Leave blank to hide this message.'),
      '#default_value' => $this->configuration['multiple_remaining_message'],
    ];
    $form['message_settings']['details']['single_remaining_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('One value remaining message'),
      '#description' => $this->t('This message is displayed when there is only one remaining value available for the element.')
      . '<br/><br/>'
      . $this->t('Leave blank to hide this message.'),
      '#default_value' => $this->configuration['single_remaining_message'],
    ];
    $form['message_settings']['details']['none_remaining_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Limit reached (zero values remaining) message'),
      '#description' => $this->t('This message is displayed when there are zero remaining values for the element.')
      . '<br/><br/>'
      . $this->t('Leave blank to hide this message.'),
      '#default_value' => $this->configuration['none_remaining_message'],
    ];
    $form['message_settings']['details']['validation_error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Validation error message'),
      '#description' => $this->t('This message is displayed when the number of values submitted exceeds the limit.'),
      '#default_value' => $this->configuration['validation_error_message'],
      '#required' => TRUE,
    ];
    $form['message_settings']['details']['placeholder_help'] = [
      '#type' => 'details',
      '#title' => $this->t('Placeholder help'),
      'description' => [
        '#markup' => $this->t('The following placeholders can be used:'),
      ],
      'items' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('@limit - The total number of values allowed for the element.'),
          $this->t('@total - The current number of values for the element.'),
          $this->t('@remaining - The remaining number of values for the element.'),
          $this->t("@label - The element's label."),
        ],
      ],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function alterElement(array &$element, FormStateInterface $form_state, array $context) {
    if (empty($element['#webform_key'])
      || $element['#webform_key'] !== $this->configuration['element_key']) {
      return;
    }

    // Set webform submission for form object.
    /** @var \Drupal\webform\WebformSubmissionForm $form_object */
    $form_object = $form_state->getFormObject();

    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $form_object->getEntity();
    $this->setWebformSubmission($webform_submission);

    $limit_info = $this->getLimitInformation();

    // Set message.
    $message_display = $this->configuration['message_display'];
    if ($message_display !== WebformEvlHandlerInterface::MESSAGE_DISPLAY_NONE) {
      $message = $this->getElementLimitStatusMessage($limit_info['status'], $limit_info);
      if ($message) {
        switch ($message_display) {
          case WebformEvlHandlerInterface::MESSAGE_DISPLAY_LABEL:
            $t_args = [
              '@label' => $element['#title'],
              '@message' => $message,
            ];
            $element['#title'] = $this->t('@label @message', $t_args);
            break;

          case WebformEvlHandlerInterface::MESSAGE_DISPLAY_DESCRIPTION:
            $element += ['#description' => ''];
            $element['#description'] .= ($element['#description']) ? '<br/>' . $message : $message;
            break;
        }
      }
    }

    // Take configured action if there are no values left.
    $element_key = $this->configuration['element_key'];
    $webform_submission = $this->getWebformSubmission();
    $existing_value = (boolean) ($webform_submission->getElementOriginalData($element_key) ?: FALSE);
    if ($limit_info['status'] === WebformEvlHandlerInterface::LIMIT_STATUS_NONE && !$existing_value) {
      switch ($this->configuration['none_remaining_action']) {
        case WebformEvlHandlerInterface::LIMIT_ACTION_DISABLE:
          $element['#disabled'] = TRUE;
          break;

        case WebformEvlHandlerInterface::LIMIT_ACTION_REMOVE:
          $element['#access'] = FALSE;
          break;
      }

      // Display limit reached message.
      $this->setElementLimitReachedMessage($element);
    }

    // If default value is not an original submission value, alter
    // to conform with what remains.
    if (isset($element['#default_value']) && empty($webform_submission->getElementOriginalData($element_key))) {
      if ($limit_info['status'] === WebformEvlHandlerInterface::LIMIT_STATUS_NONE) {
        // No values remain. Remove default value.
        unset($element['#default_value']);
      }
      elseif (count((array) $element['#default_value']) > $limit_info['remaining']) {
        // Slice off any default values that exceed what remains.
        $element['#default_value'] = array_slice((array) $element['#default_value'], 0, $limit_info['remaining']);
      }
    }

    // Add element validate callback.
    $element['#webform_evl_handler_id'] = $this->getHandlerId();
    $element['#element_validate'][] = [get_called_class(), 'validateElement'];
  }

  /**
   * Set element's limit reached message.
   *
   * @param array $element
   *   An element with limits.
   */
  protected function setElementLimitReachedMessage(array &$element) {
    if (empty($this->configuration['limit_reached_message'])) {
      return;
    }

    $args = ['@label' => $this->getElementLabel()];
    $element['#description'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => new FormattableMarkup($this->configuration['limit_reached_message'], $args),
    ];
  }

  /**
   * Validate webform element limit.
   */
  public static function validateElement(&$element, FormStateInterface $form_state, &$complete_form) {
    // Skip if element is not visible.
    if (!Element::isVisibleElement($element)) {
      return;
    }

    /** @var \Drupal\webform\WebformSubmissionForm $form_object */
    $form_object = $form_state->getFormObject();
    $webform = $form_object->getWebform();

    /** @var \Drupal\webform_evl\Plugin\WebformHandler\EvlWebformHandler $handler */
    $handler = $webform->getHandler($element['#webform_evl_handler_id']);
    $handler->validateValuesLimitedElement($element, $form_state);
  }

  /**
   * Validate an element with a values limit.
   *
   * @param array $element
   *   An element with a values limit.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @internal
   *   This method should only called by
   *   EvlWebformHandler::validateElementLimit.
   */
  public function validateValuesLimitedElement(array $element, FormStateInterface $form_state) {
    $value = (array) $form_state->getValue($this->configuration['element_key']);
    $limit_info = $this->getLimitInformation();

    if (($limit_info['total'] + count($value)) > $limit_info['limit']) {
      $message = $this
        ->getElementLimitStatusMessage(WebformEvlHandlerInterface::LIMIT_STATUS_ERROR, $limit_info);
      $form_state->setError($element, $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Must invalidate webform cache tags.
    Cache::invalidateTags(['webform:' . $this->getWebform()->id()]);
  }

  /**
   * Build summary table.
   *
   * @see \Drupal\webform_evl\Controller\WebformEvlController
   *
   * @return array
   *   A renderable containing the element values limit summary table.
   */
  public function buildSummaryTable() {
    $element = $this->getElement();
    if (!$element) {
      return [];
    }

    $limit_info = $this->getLimitInformation();
    $percentage = $limit_info['limit'] ? number_format(($limit_info['total'] / $limit_info['limit']) * 100) . '% ' : $this->t('N/A');
    $progress = [
      '#type' => 'html_tag',
      '#tag' => 'progress',
      '#attributes' => [
        'max' => $limit_info['limit'],
        'value' => $limit_info['total'],
      ],
    ];

    $row = [
      ['data' => $limit_info['limit'], 'style' => 'text-align: right'],
      ['data' => $limit_info['remaining'], 'style' => 'text-align: right'],
      ['data' => $limit_info['total'], 'style' => 'text-align: right'],
      ['data' => $progress, 'style' => 'text-align: center'],
      ['data' => $percentage, 'style' => 'text-align: right'],
    ];

    return [
      'title' => [
        '#markup' => $this->getElementLabel(),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ],
      'table' => [
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('Limit'), 'style' => 'text-align: right'],
          ['data' => $this->t('Remaining'), 'style' => 'text-align: right', 'class' => [RESPONSIVE_PRIORITY_LOW]],
          ['data' => $this->t('Total'), 'style' => 'text-align: right', 'class' => [RESPONSIVE_PRIORITY_LOW]],
          ['data' => $this->t('Progress'), 'style' => 'text-align: center', 'class' => [RESPONSIVE_PRIORITY_LOW]],
          '',
        ],
        '#rows' => [$row],
      ],
    ];
  }

  /**
   * Get the values-limited element.
   *
   * @return array
   *   The values-limited element.
   */
  protected function getElement() {
    return $this->getWebform()->getElement($this->configuration['element_key']);
  }

  /**
   * Get the webform element plugin for the values-limited element.
   *
   * @return \Drupal\webform\Plugin\WebformElementInterface|null
   *   The webform element plugin for the values-limited element.
   */
  protected function getWebformElement() {
    $element = $this->getElement();
    return ($element) ? $this->elementManager->getElementInstance($element) : NULL;
  }

  /**
   * Get values-limited element's label.
   *
   * @return string
   *   A webform element label.
   */
  protected function getElementLabel() {
    $element = $this->getElement();
    $webform_element = $this->getWebformElement();
    return $webform_element->getLabel($element);
  }

  /**
   * Get a key/value array of multi-value webform elements.
   *
   * @return array
   *   A key/value array of multi-value webform elements.
   */
  protected function getMultiValueElements() {
    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedAndFlattened();

    $options = [];
    foreach ($elements as $element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      if ($webform_element->hasMultipleValues($element)) {
        $webform_key = $element['#webform_key'];
        $t_args = [
          '@title' => $webform_element->getAdminLabel($element),
          '@type' => $webform_element->getPluginLabel(),
        ];
        $options[$webform_key] = $this->t('@title (@type)', $t_args);
      }
    }

    // Exclude elements being used by other entity value limit handlers.
    $handlers = $webform->getHandlers();
    foreach ($handlers as $handler) {
      if ($handler instanceof WebformEvlHandlerInterface
        && $handler->getHandlerId() !== $this->getHandlerId()) {
        $element_key = $handler->getSetting('element_key');
        if (isset($options[$element_key])) {
          unset($options[$element_key]);
        }
      }
    }

    return $options;
  }

  /**
   * Get limit information including limit, total, remaining, and status.
   *
   * @return array
   *   The limit information including limit, total, remaining, and status.
   */
  protected function getLimitInformation() {
    $limit = $this->configuration['limit'];
    $total = $this->getValuesTotal();

    $remaining = ($limit) ? $limit - $total : 0;

    if ($remaining <= 0) {
      $status = WebformEvlHandlerInterface::LIMIT_STATUS_NONE;
    }
    elseif ($remaining === 1) {
      $status = WebformEvlHandlerInterface::LIMIT_STATUS_SINGLE;
    }
    else {
      $status = WebformEvlHandlerInterface::LIMIT_STATUS_MULTIPLE;
    }

    return [
      'limit' => $limit,
      'total' => $total,
      'remaining' => $remaining,
      'status' => $status,
    ];
  }

  /**
   * Get the total number of values for this webform element across submissions.
   *
   * @return int
   *   The total number of values for this webform element across submissions.
   */
  protected function getValuesTotal() {
    $webform = $this->getWebform();

    $query = $this->database->select('webform_submission_data', 'sd')
      ->fields('sd', ['value'])
      ->condition('sd.name', $this->configuration['element_key'])
      ->condition('sd.webform_id', $webform->id())
      ->groupBy('sd.sid')
      ->groupBy('sd.delta');

    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get an element's limit status message.
   *
   * @param string $type
   *   Type of message.
   * @param array $limit_info
   *   Associative array containing limit, total, and remaining.
   *
   * @return \Drupal\Component\Render\FormattableMarkup|string
   *   A limit status message.
   */
  protected function getElementLimitStatusMessage($type, array $limit_info) {
    $message = $this->configuration[$type . '_message'];
    if (!$message) {
      return '';
    }

    return new FormattableMarkup($message, [
      '@label' => $this->getElementLabel(),
      '@limit' => $limit_info['limit'] ?? '',
      '@total' => $limit_info['total'] ?? '',
      '@remaining' => $limit_info['remaining'] ?? '',
    ]);
  }

}
