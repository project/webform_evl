<?php

namespace Drupal\webform_evl\Plugin;

use Drupal\webform\Plugin\WebformHandlerInterface;

/**
 * Defines the interface for webform element values limit handlers.
 */
interface WebformEvlHandlerInterface extends WebformHandlerInterface {

  /**
   * Values limit multiple remaining.
   */
  const LIMIT_STATUS_MULTIPLE = 'multiple_remaining';

  /**
   * Values limit single remaining.
   */
  const LIMIT_STATUS_SINGLE = 'single_remaining';

  /**
   * Values limit none remaining.
   */
  const LIMIT_STATUS_NONE = 'none_remaining';

  /**
   * Values limit error.
   */
  const LIMIT_STATUS_ERROR = 'validation_error';

  /**
   * Values limit action disable.
   */
  const LIMIT_ACTION_DISABLE = 'disable';

  /**
   * Values limit action remove.
   */
  const LIMIT_ACTION_REMOVE = 'remove';

  /**
   * Values limit action none.
   */
  const LIMIT_ACTION_NONE = 'none';

  /**
   * Values message label.
   */
  const MESSAGE_DISPLAY_LABEL = 'label';

  /**
   * Values message none.
   */
  const MESSAGE_DISPLAY_DESCRIPTION = 'description';

  /**
   * Values message none.
   */
  const MESSAGE_DISPLAY_NONE = 'none';

  /**
   * Build summary table.
   *
   * @return array
   *   A renderable containing the values limit summary table.
   */
  public function buildSummaryTable();

}
