<?php

namespace Drupal\Tests\webform_evl\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\Entity\Webform;

/**
 * Webform element values limit access test.
 *
 * @group webform_evl
 */
class WebformEvlAccessTest extends WebformBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'webform',
    'webform_evl',
    'webform_evl_test',
  ];

  /**
   * Test options limit access.
   */
  public function testAccess() {
    $assert_session = $this->assertSession();

    $webform = Webform::load('test_handler_evl');

    $this->postSubmission($webform);
    $this->postSubmission($webform);
    $this->postSubmission($webform);

    // Check that no one can access the values summary page.
    $this->drupalGet('/admin/structure/webform/manage/test_handler_evl/results/evl');
    $assert_session->statusCodeEquals(403);

    // Check that user with 'view any webform submission' permission can access
    // the values summary page.
    $this->drupalLogin($this->createUser(['view any webform submission']));
    $this->drupalGet('/admin/structure/webform/manage/test_handler_evl/results/evl');
    $assert_session->statusCodeEquals(200);

    // Check that options summary page is only available to webforms with
    // options limit handler.
    $this->drupalGet('/admin/structure/webform/manage/contact/results/evl');
    $assert_session->statusCodeEquals(403);
  }

}
