<?php

namespace Drupal\Tests\webform_evl\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\Entity\Webform;

/**
 * Webform element values limit test.
 *
 * @group webform_evl
 */
class WebformEvlTest extends WebformBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'webform',
    'webform_evl',
    'webform_evl_test',
  ];

  /**
   * Test element values limit.
   */
  public function testEvl() {
    $assert_session = $this->assertSession();

    $webform = Webform::load('test_handler_evl');

    $this->drupalGet('/webform/test_handler_evl');

    // Check that we see two remaining.
    $assert_session->responseContains('Custom composite (2 remaining)');

    // Post first submission.
    $sid_1 = $this->postSubmission($webform, [
      'evl_custom_composite_multiple[items][0][a]' => 'test',
      'evl_custom_composite_multiple[items][0][b]' => 'test',
    ]);

    // Check that we see one remaining.
    $assert_session->responseContains('Custom composite (1 remaining)');

    // Post second submission.
    $this->postSubmission($webform, [
      'evl_custom_composite_multiple[items][0][a]' => 'test',
      'evl_custom_composite_multiple[items][0][b]' => 'test',
    ]);

    // Check that we see zero remaining.
    $assert_session->responseContains('(0 remaining - no more Custom composite are available)');

    // Login as an admin.
    $this->drupalLogin($this->rootUser);

    // Check that existing submission values are not disabled.
    $this->drupalGet("/admin/structure/webform/manage/test_handler_evl/submission/$sid_1/edit");
    $assert_session->responseNotContains('disabled="disabled"');

    // Check that Element values limit report is available.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/structure/webform/manage/test_handler_evl/results/evl');
    $assert_session->statusCodeEquals(200);
    $assert_session->responseContains('<progress max="2" value="2"></progress>');

    // Check handler element error messages.
    $webform->deleteElement('evl_custom_composite_multiple');
    $webform->save();
    $this->drupalGet('/admin/structure/webform/manage/test_handler_evl/handlers');
    $assert_session->responseContains('<b class="color-error">\'evl_custom_composite_multiple\' is missing.</b>');
  }

}
